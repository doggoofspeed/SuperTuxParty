# Translations template for Super Tux Party.
# Copyright (C) 2019 Super Tux Party Contributors
# This file is distributed under the same license as the Super Tux Party project.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.7\n"
"PO-Revision-Date: 2021-03-08 16:02+0000\n"
"Last-Translator: Oğuz Ersen <oguzersen@protonmail.com>\n"
"Language-Team: Turkish <https://hosted.weblate.org/projects/super-tux-party/"
"minigamesknock_off/tr/>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"

msgid "KNOCK_OFF_PLAYER_WINS_MSG"
msgstr "{player} kazandı!"

msgid "KNOCK_OFF_TEAM_WINS_MSG"
msgstr "{team} takımı kazandı!"

msgid "MINIGAME_DESCRIPTION"
msgstr ""
"Her oyuncu tüm rakipleri buz dağının dışına atmaya çalışır.\n"
"Ayakta kalan son kişi kazanır!"

msgid "MINIGAME_NAME"
msgstr "Düşür"

msgid "MINIGAME_ACTION_RIGHT"
msgstr "Sağa"

msgid "MINIGAME_ACTION_LEFT"
msgstr "Sola"

msgid "MINIGAME_ACTION_DOWN"
msgstr "Aşağı"

msgid "MINIGAME_ACTION_UP"
msgstr "Yukarı"

msgid "MINIGAME_ACTION_MOVE"
msgstr "Hareket Et"
