# Translations template for Super Tux Party.
# Copyright (C) 2019 Super Tux Party Contributors
# This file is distributed under the same license as the Super Tux Party project.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.7\n"
"PO-Revision-Date: 2020-04-02 01:55+0000\n"
"Last-Translator: Tuomas Lähteenmäki <lahtis@gmail.com>\n"
"Language-Team: Finnish <https://hosted.weblate.org/projects/super-tux-party/"
"minigamesescape_from_lava/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.0-dev\n"

msgid "ESCAPE_FROM_LAVA_FINISH_MSG"
msgstr "Maali!"
