msgid ""
msgstr ""
"PO-Revision-Date: 2023-03-06 14:40+0000\n"
"Last-Translator: gallegonovato <fran-carro@hotmail.es>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/super-tux-party/"
"minigamesdungeon_parkour/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16.2-dev\n"

msgid "MINIGAME_ACTION_RIGHT"
msgstr "Moverse hacia la derecha"

msgid "MINIGAME_ACTION_LEFT"
msgstr "Moverse hacia la izquierda"

msgid "MINIGAME_ACTION_DOWN"
msgstr "Moverse hacia abajo"

msgid "MINIGAME_ACTION_UP"
msgstr "Moverse hacia arriba"

msgid "MINIGAME_ACTION_JUMP"
msgstr "Saltar"

msgid "MINIGAME_DESCRIPTION"
msgstr "Salta y corre hasta la meta"

msgid "MINIGAME_NAME"
msgstr "Parkour en la mazmorra"
